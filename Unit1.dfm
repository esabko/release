object Main: TMain
  Left = 0
  Top = 0
  Caption = #1058#1077#1089#1090#1086#1074#1086#1077' '#1079#1072#1076#1072#1085#1080#1077' ExtraLogic - '#1057#1072#1073#1082#1086' '#1045#1074#1075#1077#1085#1080#1081
  ClientHeight = 408
  ClientWidth = 903
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 903
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 376
      Top = 13
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 147
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 168
      Top = 8
      Width = 145
      Height = 25
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 41
    Width = 903
    Height = 367
    Align = alClient
    TabOrder = 1
    object cxView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Insert.Visible = False
      Navigator.Buttons.Delete.Visible = False
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.Refresh.Visible = False
      Navigator.InfoPanel.Visible = True
      Navigator.Visible = True
      FilterBox.MRUItemsListDropDownCount = 5
      OnCustomDrawCell = cxView1CustomDrawCell
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnHiding = True
      OptionsCustomize.ColumnSorting = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Indicator = True
      Styles.OnGetContentStyle = cxView1StylesGetContentStyle
      object cxView1Grp1: TcxGridDBColumn
        DataBinding.FieldName = 'Grp1'
      end
      object cxView1Grp2: TcxGridDBColumn
        DataBinding.FieldName = 'Grp2'
      end
      object cxView1Symbol: TcxGridDBColumn
        DataBinding.FieldName = 'Symbol'
      end
      object cxView1High: TcxGridDBColumn
        DataBinding.FieldName = 'High'
      end
      object cxView1Low: TcxGridDBColumn
        DataBinding.FieldName = 'Low'
      end
      object cxView1Volume: TcxGridDBColumn
        DataBinding.FieldName = 'Volume'
      end
      object cxView1QuoteVolume: TcxGridDBColumn
        DataBinding.FieldName = 'QuoteVolume'
      end
      object cxView1PercentChange: TcxGridDBColumn
        DataBinding.FieldName = 'PercentChange'
      end
      object cxView1UpdatedAt: TcxGridDBColumn
        DataBinding.FieldName = 'UpdatedAt'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxView1
    end
  end
  object SQLConn: TSQLConnection
    ConnectionName = 'SQLITECONNECTION'
    DriverName = 'Sqlite'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Sqlite'
      'Database=Base.sbd')
    Left = 56
    Top = 136
  end
  object SQLQuery: TSQLQuery
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConn
    Left = 128
    Top = 136
  end
  object DS: TClientDataSet
    PersistDataPacket.Data = {
      290100009619E0BD010000001800000009000000000003000000290104477270
      310100490000000100055749445448020002000A000447727032010049000000
      0100055749445448020002000A000653796D626F6C0100490000000100055749
      4454480200020014000448696768010049000000010005574944544802000200
      1400034C6F77010049000000010005574944544802000200140006566F6C756D
      6501004900000001000557494454480200020014000B51756F7465566F6C756D
      6501004900000001000557494454480200020014000D50657263656E74436861
      6E67650100490000000100055749445448020002000A00095570646174656441
      740100490000000100055749445448020002001E0001000D44454641554C545F
      4F524445520200820000000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Grp1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Grp2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Symbol'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'High'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Low'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Volume'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'QuoteVolume'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PercentChange'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'UpdatedAt'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end
      item
        Name = 'BySymbol'
        Fields = 'Symbol'
        Options = [ixUnique]
      end>
    IndexName = 'BySymbol'
    Params = <>
    StoreDefs = True
    Left = 56
    Top = 208
    object DSGrp1: TStringField
      FieldName = 'Grp1'
      Size = 10
    end
    object DSGrp2: TStringField
      FieldName = 'Grp2'
      Size = 10
    end
    object DSSymbol: TStringField
      FieldName = 'Symbol'
    end
    object DSHigh: TStringField
      FieldName = 'High'
      Size = 15
    end
    object DSLow: TStringField
      FieldName = 'Low'
      Size = 15
    end
    object DSVolume: TStringField
      FieldName = 'Volume'
    end
    object DSQuoteVolume: TStringField
      FieldName = 'QuoteVolume'
    end
    object DSPercentChange: TStringField
      FieldName = 'PercentChange'
      Size = 10
    end
    object DSUpdatedAt: TStringField
      FieldName = 'UpdatedAt'
      Size = 25
    end
  end
  object DataSource: TDataSource
    DataSet = DS
    Left = 128
    Top = 208
  end
end
