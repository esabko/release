unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.StrUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.FMTBcd, Vcl.StdCtrls, Vcl.ExtCtrls,
  Data.DB, Data.SqlExpr, Vcl.Grids, Vcl.DBGrids, Data.DbxSqlite, System.JSON, Data.DBXCommon,
  System.JSON.Builders, System.JSON.Types, System.JSON.Readers, System.Rtti,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinsDefaultPainters, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, Datasnap.DBClient, System.IniFiles, System.IOUtils,
  System.NetEncoding;

type
  THTTPThread = class; // Forward declaration

  TMain = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    SQLConn: TSQLConnection;
    SQLQuery: TSQLQuery;
    DS: TClientDataSet;
    DSGrp1: TStringField;
    DSSymbol: TStringField;
    DSHigh: TStringField;
    DSLow: TStringField;
    DSVolume: TStringField;
    DSQuoteVolume: TStringField;
    DSPercentChange: TStringField;
    DSUpdatedAt: TStringField;
    DataSource: TDataSource;
    cxGrid1: TcxGrid;
    cxView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Label1: TLabel;
    DSGrp2: TStringField;
    cxView1Grp1: TcxGridDBColumn;
    cxView1Grp2: TcxGridDBColumn;
    cxView1Symbol: TcxGridDBColumn;
    cxView1High: TcxGridDBColumn;
    cxView1Low: TcxGridDBColumn;
    cxView1Volume: TcxGridDBColumn;
    cxView1QuoteVolume: TcxGridDBColumn;
    cxView1PercentChange: TcxGridDBColumn;
    cxView1UpdatedAt: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cxView1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxView1StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
  private
    { Private declarations }
    PosKey: string;     // �������� ����� Symbol ����������� ����
    PosOffset: integer; // �������� �� ������� ������ �����
    PosGroup: boolean;  // ���� ��������� ������
    PosLevel: integer;  // ������� ������

    Thread: THTTPThread;
  public
    { Public declarations }
    // ������ � ��
    procedure DBInit(AFileName: string);
    procedure DBCreate;
    procedure DBClear;
    procedure DBOpen;

    // ������ � ���������� �������
    procedure PosClear;
    procedure PosSaveINI;
    procedure PosLoadINI;
    procedure PosSaveUpd;
    procedure PosLoadUpd;

    // ������ � INI
    procedure INISave;
    procedure INILoad;
  end;

  THTTPThread = class(TThread)
    HTTPResult: string;
    HTTPDate: TDateTime;
    FirstRun: boolean;
    procedure Execute; override;
    procedure SleepUntil(D: TDateTime; MSec: integer);
    function GetSimpleHTTPS(AURL: string): string;
    procedure UpdateResults;

    // ������ � ��
    procedure DBFillJSON(s: string); // ��������� Sqlite �� JSON
    procedure DBUpdate;
    procedure DBInsertOrReplace(ASymbol, AHigh, ALow, AVolume, AQuoteVolume, APercentChange, AUpdatedAt: string);

  end;

  function iif(ACond: boolean; ATrue, AFalse: string): string;
  function NormalizeFileName(AFileName: string): string;
  function MilliSecondsBetween(const ANow, AThen: TDateTime): integer;
  function Min2Int(const A, B: integer): integer;

var
  Main: TMain;

implementation

{$R *.dfm}

uses
  IdHTTP, IdSSLOpenSSL;

//------------------------------------------------------------------------------

function iif(ACond: boolean; ATrue, AFalse: string): string;
begin
  if ACond then Result := ATrue else Result := AFalse;
end;

function NormalizeFileName(AFileName: string): string;
var
  Path: string;
begin
  if Length(AFileName) > 1 then begin
    if (Copy(AFileName, 2, 1) = ':') or (Copy(AFileName, 1, 2) = '\\') then begin
      Result := AFileName;
      Exit;
    end;
  end;
  Path := ExtractFilePath(Application.ExeName);
  Result := Path + iif(Copy(Path, Length(Path), 1) <> '\', '\', '') + AFileName;
end;

function MilliSecondsBetween(const ANow, AThen: TDateTime): integer;
begin
  Result := Trunc((ANow - AThen) * 86400000);
end;

function Min2Int(const A, B: integer): integer;
begin
  if A < B then Result := A else Result := B;
end;

//------------------------------------------------------------------------------

procedure THTTPThread.SleepUntil(D: TDateTime; MSec: integer);
var
  i, Delta: integer;
begin
  repeat
    if Terminated then Exit;
    i := MilliSecondsBetween(Now, D);
    if i > MSec then Exit;
    Delta := MSec - i;
    Sleep(Min2Int(500, Delta));
  until False;
end;

procedure THTTPThread.Execute;
begin
  FirstRun := True;
  HTTPDate := Now;
  HTTPResult := '';
  Synchronize(UpdateResults);
  repeat
    if Terminated then Break;
    HTTPDate := Now;
    HTTPResult := GetSimpleHTTPS('https://api.bittrex.com/v3/markets/summaries');
    Synchronize(UpdateResults);
    SleepUntil(HTTPDate, 10000);
  until False;
end;

function THTTPThread.GetSimpleHTTPS(AURL: string): string;
var
  IdHTTP : TIdHTTP;
  Id_HandlerSocket : TIdSSLIOHandlerSocketOpenSSL;
begin
  IdHTTP := TIdHTTP.Create(Nil);
  Id_HandlerSocket := TIdSSLIOHandlerSocketOpenSSL.Create(IdHTTP);
  try
    try
      IdHTTP.Request.BasicAuthentication := False;
      IdHTTP.Request.UserAgent := 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0';
      Id_HandlerSocket.SSLOptions.Mode := sslmClient;
      Id_HandlerSocket.SSLOptions.Method := sslvSSLv23;
      IdHTTP.IOHandler := Id_HandlerSocket;
      Result := IdHTTP.Get(AURL);
    except
      Result := '';
      //on E: EIdHTTPProtocolException do Result := E.ErrorMessage;
    end;
  finally
    Id_HandlerSocket.Free;
    IdHTTP.Free;
  end;
end;

procedure THTTPThread.UpdateResults;
begin
  if HTTPResult <> '' then begin
    Main.Label1.Caption := DateTimeToStr(HTTPDate);
    Main.Label1.Font.Color := clWindowText;
    DBFillJSON(HTTPResult);
    end
  else begin
    Main.Label1.Font.Color := clRed;
  end;
  if Main.Label1.Caption = '' then Main.Label1.Caption := DateTimeToStr(HTTPDate);
  DBUpdate;
  if FirstRun then FirstRun := False;
end;

procedure THTTPThread.DBInsertOrReplace(ASymbol, AHigh, ALow, AVolume, AQuoteVolume, APercentChange, AUpdatedAt: string);
var
  Grp1, Grp2, Upd, Cols, Vals: string;
  p: integer;
begin
  p := Pos('-', ASymbol);
  Grp1 := iif(p > 0, Copy(ASymbol, 1, p - 1), ASymbol);
  Grp2 := Copy(ASymbol, p + 1, Length(ASymbol));
  Upd := Copy(ReplaceStr(AUpdatedAt, 'T', ' '), 1, 19);
  Cols := 'Grp1, Grp2, Symbol, High, Low, Volume, QuoteVolume, PercentChange, UpdatedAt';
  Vals := '"' + Grp1 + '", "' + Grp2 + '", "' + ASymbol + '", "' +
          AHigh + '", "' + ALow + '", "' + AVolume + '", "' +
          AQuoteVolume + '", "' + APercentChange + '", "' + Upd + '"';
  Main.SQLQuery.SQL.Text := 'INSERT OR REPLACE into Markets (' + Cols + ') VALUES (' + Vals + ') ' +
                        'ON CONFLICT (Symbol) DO UPDATE SET ' +
                        'Grp1 = "' + Grp1 + '", ' +
                        'Grp2 = "' + Grp2 + '", ' +
                        'Symbol = "' + ASymbol + '", ' +
                        'High = "' + AHigh + '", ' +
                        'Low = "' + ALow + '", ' +
                        'Volume = "' + AVolume + '", ' +
                        'QuoteVolume = "' + AQuoteVolume + '", ' +
                        'PercentChange = "' + APercentChange + '", ' +
                        'UpdatedAt = "' + Upd + '" ' +
                        'WHERE Symbol = "' + ASymbol + '";';
  Main.SQLQuery.ExecSQL;
end;

procedure THTTPThread.DBFillJSON(s: string);
var
  T: TDBXTransaction;
  FSymbol, FUpdatedAt, FHigh, FLow, FVolume, FQuoteVolume, FPercentChange: string;
  JSON: TJSONIterator;
  Reader: TJsonTextReader;
  TextReader: TStringReader;
begin
  TextReader := TStringReader.Create(S);
  Reader := TJsonTextReader.Create(TextReader);
  JSON := TJSONIterator.Create(Reader);
  try
    try
      T := Main.SQLConn.BeginTransaction;
      JSON.Next;
      while JSON.Next do begin
        if JSON.&Type = TJsonToken.StartObject then begin
          FSymbol := '';
          FUpdatedAt := '';
          FHigh := '';
          FLow := '';
          FVolume := '';
          FQuoteVolume := '';
          FPercentChange := '';

          JSON.Recurse;
          while JSON.Next do begin
            //ShowMessage('Key = ' + JSON.Key + ' Val = ' + JSON.AsString);
            if AnsiUpperCase(JSON.Key) = 'SYMBOL' then begin
              FSymbol := JSON.AsString;
              Continue;
            end;
            if AnsiUpperCase(JSON.Key) = 'HIGH' then begin
              FHigh := JSON.AsString;
              Continue;
            end;
            if AnsiUpperCase(JSON.Key) = 'LOW' then begin
              FLow := JSON.AsString;
              Continue;
            end;
            if AnsiUpperCase(JSON.Key) = 'VOLUME' then begin
              FVolume := JSON.AsString;
              Continue;
            end;
            if AnsiUpperCase(JSON.Key) = 'QUOTEVOLUME' then begin
              FQuoteVolume := JSON.AsString;
              Continue;
            end;
            if AnsiUpperCase(JSON.Key) = 'PERCENTCHANGE' then begin
              FPercentChange := JSON.AsString;
              Continue;
            end;
            if AnsiUpperCase(JSON.Key) = 'UPDATEDAT' then begin
              FUpdatedAt := JSON.AsString;
              Continue;
            end;
          end;
          // ������ PercentChange ���������� � JSON
          DBInsertOrReplace(FSymbol, FHigh, FLow, FVolume, FQuoteVolume, FPercentChange, FUpdatedAt);
          JSON.Return;
        end;
      end;

      Main.SQLConn.CommitFreeAndNil(T);
    except
      Main.SQLConn.RollbackFreeAndNil(T);
      ShowMessage('ERR');
      raise;
    end;
  finally
    JSON.Free;
    Reader.Free;
    TextReader.Free;
  end;
end;

procedure THTTPThread.DBUpdate;
var
  i: integer;
  Flag: boolean;
begin
  try
    // ��������� ���������� ������
    Main.PosSaveUpd;
    Main.DS.DisableControls;
    Main.SQLQuery.SQL.Text := 'select * from Markets order by Symbol;';
    Main.SQLQuery.Open;
    Main.SQLQuery.First;
    while not Main.SQLQuery.Eof do begin
      if Main.DS.FindKey([Main.SQLQuery.Fields[2].AsString]) then begin
        // ����������
        Flag := False;
        for i := Main.DS.FieldCount - 1 downto 0 do begin
          if Main.DS.Fields[i].AsString <> Main.SQLQuery.Fields[i].AsString then begin
            Flag := True;
            Break;
          end;
        end;
        if Flag then begin
          Main.DS.Edit;
          for i := 0 to Main.DS.FieldCount - 1 do begin
            Main.DS.Fields[i].AsString := Main.SQLQuery.Fields[i].AsString;
          end;
          Main.DS.Post;
        end;
        end
      else begin
        // ����������
        Main.DS.Insert;
        for i := 0 to Main.DS.FieldCount - 1 do begin
          Main.DS.Fields[i].AsString := Main.SQLQuery.Fields[i].AsString;
        end;
        Main.DS.Post;
      end;
      Main.SQLQuery.Next;
    end;
    // ������������ ���������� ������
    if FirstRun then begin
      Main.DS.First;
      Main.DS.EnableControls;
      Main.INILoad;
      end
    else begin
      Main.PosLoadUpd;
    end;
  finally
    if Main.SQLQuery.Active then Main.SQLQuery.Close;
    Main.DS.EnableControls;
  end;
  Main.ActiveControl := Main.cxGrid1;
end;

//------------------------------------------------------------------------------

procedure TMain.DBOpen;
begin
  DBInit('Base.sbd');
end;

procedure TMain.DBInit(AFileName: string);
begin
  SQLConn.ConnectionName := 'SQLITECONNECTION';
  SQLConn.DriverName := 'Sqlite';
  SQLConn.LoginPrompt := False;
  SQLConn.Params.Values['Host'] := 'localhost';
  SQLConn.Params.Values['FailIfMissing'] := 'False';
  SQLConn.Params.Values['ColumnMetaDataSupported'] := 'False';
  SQLConn.Params.Values['Database'] := NormalizeFileName(AFileName);
  SQLConn.Open;
  DBCreate;
end;

procedure TMain.DBClear;
var
  T: TDBXTransaction;
begin
  T := SQLConn.BeginTransaction;
  SQLQuery.SQL.Text := 'delete from Markets;';
  try
    SQLQuery.ExecSQL;
    SQLConn.CommitFreeAndNil(T);
  except
    SQLConn.RollbackFreeAndNil(T);
  end;
end;

procedure TMain.DBCreate;
begin
  SQLQuery.SQL.Text := 'CREATE TABLE if not exists Markets ( '+
                    'Grp1 CHAR(10),'+
                    'Grp2 CHAR(10),'+
                    'Symbol CHAR(20) primary key,'+
                    'High TEXT,'+
                    'Low TEXT,'+
                    'Volume TEXT,'+
                    'QuoteVolume TEXT,'+
                    'PercentChange TEXT,'+
                    'UpdatedAt TEXT'+
                    ');';
  SQLQuery.ExecSQL;
end;

//------------------------------------------------------------------------------

procedure TMain.PosClear;
begin
  PosKey := '';
  PosOffset := 0;
  PosGroup := False;
  PosLevel := 0;
end;

procedure TMain.PosSaveINI;
var
  i: integer;
  Grp: TStringList;
begin
  PosClear;
  if DS.RecordCount <> 0 then begin
    PosOffset := cxView1.Controller.FocusedRecord.RecordIndex - cxView1.Controller.TopRecordIndex;
    PosGroup := cxView1.Controller.FocusedRecord.Expandable;
    PosLevel := cxView1.Controller.FocusedRecord.Level;
    if PosGroup then begin
      Grp := TStringList.Create;
      try
        for i := 0 to PosLevel do begin
          Grp.Add(DS.Fields[cxView1.GroupedColumns[i].Index].AsString);
        end;
        PosKey := Grp.CommaText;
      finally
        Grp.Free;
      end;
      end
    else begin
      PosKey := DS.Fields[2].AsString; // �������� ���� �� ������� ������
    end;
  end;
end;

procedure TMain.PosLoadINI;
var
  i, j, CountRow, CountGrp, Beg: integer;
  Grp: TStringList;
  VData: TcxGridViewData;
  Group: string;
begin
  if PosKey <> '' then begin
    if PosGroup = False then begin
      DS.FindNearest([PosKey]);
      while cxView1.Controller.FocusedRecord.Expandable do begin
        cxView1.Controller.FocusRecord(cxView1.Controller.FocusedRowIndex + 1, True);
      end;
      end
    else begin
      Grp := TStringList.Create;
      Grp.CommaText := PosKey;
      CountGrp := Grp.Count;
      VData := cxView1.ViewData;
      CountRow := VData.RowCount;
      Beg := 0;
      try
        for j := 0 to CountGrp - 1 do begin
          Group := Grp[j];
          for i := Beg to CountRow - 1 do begin
            if VData.Rows[i].Level <> j then Continue;
            if VData.Rows[i].Values[j] <> Group then Continue;
            VData.Rows[i].Focused := True;
            Beg := i + 1;
            Break;
          end;
        end;
      finally
        Grp.Free;
      end;
    end;
    i := cxView1.Controller.FocusedRecord.RecordIndex - PosOffset;
    if i < 0 then i := 0;
    cxView1.Controller.TopRecordIndex := i;
  end;
end;

procedure TMain.PosSaveUpd;
begin
  PosClear;
  if DS.RecordCount <> 0 then begin
    PosKey := DS.Fields[2].AsString; // �������� ���� �� ������� ������
    PosOffset := cxView1.Controller.FocusedRecord.RecordIndex - cxView1.Controller.TopRecordIndex;
    PosGroup := cxView1.Controller.FocusedRecord.Expandable;
  end;
end;

procedure TMain.PosLoadUpd;
var
  i: integer;
begin
  DS.FindNearest([PosKey]);
  if PosKey <> '' then begin
    if PosGroup = False then begin
      if cxView1.Controller.FocusedRecord.Expandable then begin
        cxView1.Controller.FocusRecord(cxView1.Controller.FocusedRowIndex + 1, True);
      end;
    end;
    i := cxView1.Controller.FocusedRecord.RecordIndex - PosOffset;
    if i < 0 then i := 0;
    cxView1.Controller.TopRecordIndex := i;
  end;
end;

//------------------------------------------------------------------------------

procedure TMain.INISave;
var
  INI: TIniFile;
  i, Count: integer;
  VData: TcxGridViewData;
  Exp: TStringList;
begin
  // ���������� ��������
  INI := TIniFile.Create(TPath.ChangeExtension(Application.ExeName, 'ini'));
  Exp := TStringList.Create;
  try
    // ��������� ��������� ������
    VData := cxView1.ViewData;
    Count := VData.RowCount;
    for i := 0 to Count - 1 do begin
      //if VData.Rows[i].Expandable and (VData.Rows[i].Level = 0) and VData.Rows[i].Expanded then begin
      //  Exp.Add(VData.Rows[i].Values[0]);
      //end;
      // ����� ������ ����������, ��������������
      if VData.Rows[i].Expandable and VData.Rows[i].Expanded then begin
        Exp.Add(IntToStr(VData.Rows[i].Level) + ':' + VData.Rows[i].Values[0]);
      end;
    end;
    INI.WriteString('Common', 'Expanded', Exp.CommaText);

    // ��������� ���������� ������
    PosSaveINI;
    INI.WriteString('Common', 'Focused', PosKey);
    INI.WriteInteger('Common', 'FocusedOffset', PosOffset);
    INI.WriteBool('Common', 'FocusedGroup', PosGroup);
    INI.WriteInteger('Common', 'FocusedLevel', PosLevel);

    // ��������� ��������� �������
    Count := cxView1.ColumnCount;
    for i := 0 to Count - 1 do
    begin
      INI.WriteBool(cxView1.Columns[i].Name, 'Visible', cxView1.Columns[i].Visible);
      INI.WriteInteger(cxView1.Columns[i].Name, 'Position', cxView1.Columns[i].Index);
      INI.WriteInteger(cxView1.Columns[i].Name, 'GroupIndex', cxView1.Columns[i].GroupIndex);
      INI.WriteInteger(cxView1.Columns[i].Name, 'Width', cxView1.Columns[i].Width);
    end;
  finally
    Exp.Free;
    INI.Free;
  end;
  ActiveControl := cxGrid1;
end;

procedure TMain.INILoad;
var
  INI: TIniFile;
  i, j, Count, CountExp, CountRow, Level, p: integer;
  s, Group: string;
  Sections: TStringList;
  Item: TcxCustomGridTableItem;
  VData: TcxGridViewData;
  Exp: TStringList;
begin
  // �������� ��������
  INI := TIniFile.Create(TPath.ChangeExtension(Application.ExeName, 'ini'));
  Exp := TStringList.Create;
  Sections := TStringList.Create;
  try
    cxView1.DataController.Filter.Active := False;
    // ��������� ��������� �������
    INI.ReadSections(Sections);
    Count := Sections.Count;
    for i := 0 to Count - 1 do
    begin
      Item := cxView1.FindItemByName(Sections[i]);
      if Assigned(Item) then begin
        Item.Index := INI.ReadInteger(Sections[i], 'Position', Item.Index );
        TcxGridColumn(Item).GroupIndex := INI.ReadInteger(Sections[i], 'GroupIndex', TcxGridColumn(Item).GroupIndex);
        TcxGridColumn(Item).Width := INI.ReadInteger(Sections[i], 'Width', TcxGridColumn(Item).Width);
        Item.Visible := INI.ReadBool(Sections[i], 'Visible', True );
      end;
    end;

    // ��������������� ��������� ������
    // ����� ������, ��������������
    cxView1.DataController.Groups.FullCollapse;
    Exp.CommaText := INI.ReadString('Common', 'Expanded', '');
    VData := cxView1.ViewData;
    CountRow := VData.RowCount;
    CountExp := Exp.Count;
    if (cxView1.GroupedColumnCount > 0) and (CountExp > 0) and (CountRow > 0) then begin
      i := 0;
      j := 0;
      repeat
        if (i > CountRow - 1) or (j > CountExp - 1) then Break;
        s := Exp[j];
        p := Pos(':', s);
        Level := StrToInt(Copy(s, 1, p - 1));
        Group := Copy(s, p + 1, Length(s));
        if VData.Rows[i].Expandable and (VData.Rows[i].Level = Level) and (VData.Rows[i].Values[Level] = Group) then begin
          VData.Rows[i].Expanded := True;
          CountRow := VData.RowCount;
          Inc(j);
        end;
        Inc(i);
      until False;
    end;

    // ��������������� ���������� ������
    PosKey := INI.ReadString('Common', 'Focused', '');
    PosOffset := INI.ReadInteger('Common', 'FocusedOffset', 0);
    PosGroup := INI.ReadBool('Common', 'FocusedGroup', False);
    PosLevel := INI.ReadInteger('Common', 'FocusedLevel', 0);
    PosLoadINI;

  finally
    Sections.Free;
    Exp.Free;
    INI.Free;
  end;
  ActiveControl := cxGrid1;
end;

//------------------------------------------------------------------------------

procedure TMain.FormCreate(Sender: TObject);
begin
  ActiveControl := cxGrid1;
  PosClear;
  Label1.Caption := '';
  DBOpen;

  Thread := THTTPThread.Create(True); // ��������� �� �����, ����� ��������� ������ ��������
  Thread.FreeOnTerminate := False;    // ��� ���������� �� �������
  //Thread.Priority := tpLower;         // ��������� ������ ���������
  Thread.Start;
end;

procedure TMain.FormDestroy(Sender: TObject);
begin
  INISave;
  Thread.Terminate;
  Thread.WaitFor;
  Thread.Free;
  Thread := nil;
end;

procedure TMain.Button1Click(Sender: TObject);
begin
  INISave;
end;

procedure TMain.Button2Click(Sender: TObject);
begin
  INILoad;
end;

procedure TMain.cxView1CustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  Val: string;
  ARec: TRect;
begin
  // ��������� ����
  // ������ ��� ���������� ������
  if not AViewInfo.RecordViewInfo.Focused then Exit;
  if AViewInfo.Item.Index <> 7 then Exit; // ������ ������ PercentChange
  if AViewInfo.Item.Focused then Exit;
  Val := AViewInfo.Value;
  if (Val = '') or (Val = '0') then begin
    ACanvas.Canvas.Brush.Color := clGray;
    end
  else begin
    if Val[1] = '-' then begin
      ACanvas.Canvas.Brush.Color := clRed;
      end
    else begin
      ACanvas.Canvas.Brush.Color := clGreen;
    end;
  end;
  ARec := AViewInfo.Bounds;
  ACanvas.Canvas.FillRect(ARec);
end;

procedure TMain.cxView1StylesGetContentStyle(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  var AStyle: TcxStyle);
var
  Val: string;
begin
  // ��������� ����
  // ������ ����� ������������ �����
  if ARecord = nil then Exit;
  if AItem = nil then Exit;
  if not ARecord.IsData then Exit;
  if AItem.Index <> 7 then Exit; // ������ ������ PercentChange

  Val := ARecord.Values[7];
  if not Assigned(AStyle) then AStyle := TcxStyle.Create(Sender);
  if not ARecord.Focused or AItem.Focused then begin
    if (Val = '') or (Val = '0') then begin
      AStyle.TextColor := clGray;
      end
    else begin
      if Val[1] = '-' then begin
        AStyle.TextColor := clRed;
        end
      else begin
        AStyle.TextColor := clGreen;
      end;
    end;
  end;
end;

end.
